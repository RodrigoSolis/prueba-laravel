@extends('layouts.app')

@section('content')
<div class="container">
<h1>Listado de usuarios</h1>
<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Agregar</button>
<br><br>   
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($usuarios as $i=> $usuario)
            <tr>
                <td>{{$i+1}}</td>
                <td>{{$usuario->name}}</td>
                <td>{{$usuario->email}}</td>
                <td>
                    <a href="/ver/{{$usuario->id}}" class="btn btn-success btn-xs">Ver</a>
                    <a  style="color:white"href="/eliminar/{{$usuario->id}}" class="btn btn-xs btn-danger">Eliminar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar nuevo usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-usuario">
      {{csrf_field()}}
      <div class="modal-body">
        <div class="row">
            <div class="col">
                <input type="text" name="nombre"  id="form-nombre" class="form-control" placeholder="Nombre">
            </div>
            <div class="col">
                <input type="email" name="email" id="form-email" class="form-control" placeholder="Email">
            </div>
        </div><br>
        <div class="row">
            <div class="col">
                <input type="password" name="password" id="form-password" class="form-control" placeholder="Password">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnguardar" class="btn btn-primary">Guardar</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>

    $(document).ready(function(){
        $('#btnguardar').click(function(){
            if($('#form-nombre').val() == '' || $('#form-email').val() == '' || $('#form-password').val() == ''){
                alert('Debe completar todos los campos');
            }else{
                $.ajax({
                    url: 'guardar',
                    type: 'POST',
                    data: $('#form-usuario').serialize(),
                    dataType: 'json',
                    success: function(result){
                        console.log(result);
                        location.reload();
                    },
                    error: function(result){
                        console.log(result);
                    }
                });
            }
        });
    });

</script>
@endsection