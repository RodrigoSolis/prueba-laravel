@extends('layouts.app')

@section('content')
<div class="container">
<h1>Usuario</h1>
    <form action="editar" type="post">
    <input type="hidden" name="id" value="{{$usuario->id}}">
        <div class="row">
            <div class="col">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="{{$usuario->name}}">
            </div>
            <div class="col">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control" placeholder="Email" value="{{$usuario->email}}">
            </div>
        </div>
        <br>
        <input type="submit" class="btn btn-primary btn-lg" value="Actualizar">
    </form>
</div>
@endsection
