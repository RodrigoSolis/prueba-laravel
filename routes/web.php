<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('usuarios',[
    'uses' => 'HomeController@usuarios',
    'as' => 'usuarios'
]);

Route::get('ver/{id}',[
    'uses' => 'HomeController@Ver',
    'as' => 'ver'
]);

Route::get('editar',[
    'uses' => 'HomeController@Editar',
    'as' => 'editar'
]);

Route::post('guardar',[
    'uses' => 'HomeController@Guardar',
    'as' => 'guardar'
]);

Route::get('eliminar/{id}',[
    'uses' => 'HomeController@Eliminar',
    'as' => 'eliminar'
]);