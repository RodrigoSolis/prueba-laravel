<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function Usuarios(){
        $usuarios = User::all();
        return view('usuarios',compact('usuarios'));
    }

    public function Ver($id){
        $usuario = User::where('id',$id)->first();
        return view('usuario',compact('usuario'));
    }

    public function Editar(Request $request){
        $usuario = User::find($request->id);
        $usuario->name = $request->nombre;
        $usuario->email = $request->email;
        $usuario->update();
        return redirect('/home');
    }

    public function Guardar(Request $request){
        $usuario = new User;
        $usuario->name = $request->nombre;
        $usuario->email = $request->email;
        $usuario->password = bcrypt($request->password);
        $usuario->save();
        return json_encode(true);
    }

    public function Eliminar($id){
        $usuario = User::find($id);
        $usuario->delete();
        return redirect('usuarios');
    }
}
